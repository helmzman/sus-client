import requests
import json
from datetime import datetime

def register(url, userid, public_key):
    response = requests.post(url + "register",
                  headers={"Content-Type": "application/json"},
                  data=json.dumps({"userid": userid, "public_key": public_key}))
    if response.status_code == 200:
        print(response.text)
        return userid
    else:
        # TODO: If the public_key is the same as the already registered user's
        # public key, we should consider this user registered
        print(f"Error: {response.text}")
        return None

def get_key(url, userid):
    response = requests.post(url + "get_key",
                  headers={"Content-Type": "application/json"},
                  data=json.dumps({"userid": userid}))
    if response.status_code == 200:
        j = json.loads(response.text)
        print(f"Returned key for user {userid}: {j['public_key']}")
    else:
        print(f"Error: {response.text}")

def send(url, senderid, recipientid, encrypted_text):
    response = requests.post(url + "send",
                  headers={"Content-Type": "application/json"},
                  data=json.dumps({"senderid": senderid, "recipientid": recipientid, "encrypted_text": encrypted_text}))
    if response.status_code == 200:
        print(response.text)
    else:
        print(f"Error: {response.text}")

def get_chats(url, userid):
    response = requests.post(url + "get_chats",
                  headers={"Content-Type": "application/json"},
                  data=json.dumps({"userid": userid}))
    if response.status_code == 200:
        senders = json.loads(response.text)["senders"]
        print("Senders:")
        for sender in senders:
            print(f"\t- {sender}")
        #j = json.loads(response.text)
        #print(f"Returned key for user {userid}: {j['public_key']}")
    else:
        print(f"Error: {response.text}")

def get_messages(url, userid, senderid):
    response = requests.post(url + "get_messages",
                  headers={"Content-Type": "application/json"},
                  data=json.dumps({"userid": userid, "senderid": senderid}))
    if response.status_code == 200:
        for message in json.loads(response.text)["messages"]:
            print(f"#{datetime.fromtimestamp(message['timestamp']/1000.0)}: ")
            print(message["encrypted_text"])
            print()
    else:
        print(f"Error: {response.text}")