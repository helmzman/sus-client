from pathlib import Path 
from utils import *
import os, sys

sys.path.append(os.path.relpath("./pyky"))
import ccakem

def input_int(prompt):
    i = input(prompt)
    try:
        i = int(i)
    except ValueError:
        i = None
    return i

def input_int_in_range(prompt, min=None, max=None):
    if min is None or max is None:
        i = input(prompt)
        try:
            i = int(i)
        except ValueError:
            i = None
        return i
    else:
        while True:
            i = input(prompt)
            try:
                i = int(i)
                if i < min or i > max:
                    continue
                return i
            except ValueError:
                continue

def input_path(prompt, default_path="~/.sus/id"):
    pathstr = input(prompt)
    if pathstr.strip() == "":
        return Path(os.path.expanduser(default_path))
    else:
        return Path(os.path.expanduser(pathstr))


def save_key_to_path(path: Path, private_key, public_key):
    try:
        # TODO: Should set appropriate permissions for these files
        path.parent.mkdir(exist_ok=True, parents=True)
        priv_key_bytes = bytes((x % 256 for x in private_key))
        path.write_bytes(priv_key_bytes)

        pub_key_bytes = bytes((x % 256 for x in public_key))
        pub_path = Path(str(path.resolve())+'.pub')
        pub_path.write_bytes(pub_key_bytes)

        return True
    except:
        return False

def load_key_from_path(path: Path):
    try:
        pub_path = Path(str(path.resolve())+'.pub')
        priv_key_bytes = path.read_bytes()
        pub_key_bytes = pub_path.read_bytes()

        return bytes_to_signed_ints(priv_key_bytes), bytes_to_signed_ints(pub_key_bytes)
    except:
        return None, None

def tui_generate_keypair():
    print("""Before connecting, you need to generate or load a public/private
key pair.  Please make a selection:
    1. Generate a new Kyber public/private key pair
    2. Load the key pair from a file""")
    choice = input_int_in_range("> ", 1, 2)
    if choice == 1:
        print("Generating a new Kyber key pair...")
        private_key, public_key = ccakem.kem_keygen512()
        path = input_path("Enter file in which to save the key (default ~/.sus/id): ")
        if save_key_to_path(path, private_key, public_key):
            print(f"Successfully wrote private key and public key to {path} and {path.__str__() + '.pub'}")
    elif choice == 2:
        path = input_path("Enter path to load the keypair from print (default ~/.sus/id): ")
        private_key, public_key = load_key_from_path(path)
        if private_key is None or public_key is None:
            print("Failed to load the keypair.")
        else:
            print("Successfully loaded the private key and public key")
    return private_key, public_key