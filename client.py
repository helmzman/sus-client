import requests
import json
from datetime import datetime
from Cryptodome.Cipher import AES
from Cryptodome.Util.Padding import pad, unpad
from Cryptodome.Random import get_random_bytes
import sys, os
from pathlib import Path
from base64 import b64encode, b64decode

# Local
from utils import *
from io_utils import *
from api_wrappers import *
from api import *


# This has to be done so that we can import the kyber methods
sys.path.append(os.path.relpath("./pyky"))

import ccakem

# TODO: This really shouldn't be HTTP
URL = "http://127.0.0.1:1337/"

# TODO: Should have some way to set these, especially when we are actually using
# encryption
user = ""
public_key = ""
private_key = ""
session_keys = {}

def aes_encrypt(secret, msg):
    key = bytes((x % 256 for x in secret))
    padded_plaintext = pad(str.encode(msg), AES.block_size)
    iv = get_random_bytes(AES.block_size)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    ciphertext = iv + cipher.encrypt(padded_plaintext)
    return ciphertext

def aes_decrypt(secret, ciphertext):
    key = bytes((x % 256 for x in secret))
    iv = ciphertext[:AES.block_size]
    encrypted_message = ciphertext[AES.block_size:]
    cipher = AES.new(key, AES.MODE_CBC, iv)
    decrypted_padded_plaintext = cipher.decrypt(encrypted_message)
    decrypted_plaintext = unpad(decrypted_padded_plaintext, AES.block_size)
    return decrypted_plaintext

def is_user_registered(user1):
    if user1 == "" or api_get_key(URL, user1) is None:
        return False
    return True

def has_chat_started(user1, user2):
    # Get chats for user1 and user2
    # if user2 not in user1 chats and user1 not in user2 chats: False, else True

    user1_chats = api_get_chats(URL, user1)
    user2_chats = api_get_chats(URL, user2)
    if user2 not in user1_chats and user1 not in user2_chats:
        return False
    return True

# Derives a session key from existing messages
def find_session_key(user1, user2):
    # Assumes these two users already have a chat

    # We are only able to derive the session key as user1 if user2 was the one
    # to initiate communications
    user2_sent = api_get_messages(URL, user1, user2) or []
    user1_sent = api_get_messages(URL, user2, user1) or []

    # Find first message sent - this should contain the session key
    earliest_message = None
    if not user1_sent and not user2_sent:
        # Error condition
        return None
    elif user1_sent and not user2_sent:
        # Cannot derive the session_key
        return None
    elif user2_sent and not user1_sent:
        earliest_message = user2_sent[0]    # Assumes order
    else:
        first_user1 = user1_sent[0]
        first_user2 = user2_sent[0]
        if first_user1['timestamp'] <= first_user2['timestamp']:
            # Cannot derive the session key 
            return None
        else:
            earliest_message = first_user2
    
    # Derive session_key from earliest message
    sk_enc = earliest_message['encrypted_text']
    return ccakem.kem_decaps512(private_key, sk_enc)

# Generates and sends a session key to user2
# Returns the session key; None if there was an error
def send_session_key(user1, user2):
    # Get user2's public key 
    user2_public_key = api_get_key(URL, user2)
    if user2_public_key is None:
        return None
    print(user2_public_key) # TESTING

    # Generate session_key and cipher containing it
    session_key, cipher = ccakem.kem_encaps512(user2_public_key)
    
    # Send the cipher to user2
    res = api_send(URL, user1, user2, cipher)
    return session_key if res else None

# Tries to get the session key for a chat between user1 and user2 checking
# session_keys and find_session_key
def get_session_key(user1, user2):
    if has_chat_started(user1, user2):
        # Find session key
        # Check session_keys
        if user2 in session_keys:
            session_key = session_keys[user2]
        else:
            # This can still fail if userid was the one to initiate
            session_key = find_session_key(user1, user2)
            # TODO: Handle this error differently.  Occurs when the session key
            # is lost.
            if not session_key: return None
            session_keys[user2] = session_key
        return session_key
    return None 

def send_message(recipientid, message):
    global user

    # Has chat started?
    #   True: Find session key
    #   False: Create and send session key
    # Use session key to encrypt the message and send it 
    if not is_user_registered(user):
        print("Please register before sending messages")
        return False
    elif not is_user_registered(recipientid):
        print(f"User {recipientid} is not registered")
        return False

    # Tries to get the session key from an existing chat
    session_key = get_session_key(user, recipientid)
    if not session_key:
        # Generate and send a new session key to start a chat
        session_key = send_session_key(user, recipientid)
        if not session_key: return None 
        session_keys[recipientid] = session_key
    
    # At this point, the chat has been started and we have the session key
    message_enc = aes_encrypt(session_key, message)
    res = api_send(URL, user, recipientid, b64encode(message_enc).decode('ascii'))
    return res

def decrypt_message(msg, session_key):
    # Ignore session key message
    if isinstance(msg["encrypted_text"], list):
        return None
    return aes_decrypt(session_key, b64decode(msg["encrypted_text"]))

def decrypt_and_print_message(user, msg, session_key):
    decrypted_message = decrypt_message(msg, session_key)
    if decrypted_message is None:
        return
    print(f"{user}: {decrypted_message}")

def view_chat_messages(user2):
    global user

    # Get all messages user1->user2 and user2->user1
    # Print out the messages in order of ascending timestamps
    # Decrypt the message text before printing

    # Find session key
    if (session_key := get_session_key(user, user2)) is None:
        print("Chat doesn't exist or error retrieving session key")
        return
    
    user2_sent = api_get_messages(URL, user, user2) or []
    user1_sent = api_get_messages(URL, user2, user) or []

    u2i = 0
    u1i = 0
    while u2i < len(user2_sent) and u1i < len(user1_sent):
        msg = None
        if user2_sent[u2i]['timestamp'] <= user1_sent[u1i]['timestamp']:
            decrypt_and_print_message(user2, user2_sent[u2i], session_key)
            u2i += 1
        elif user2_sent[u2i]['timestamp'] > user1_sent[u1i]['timestamp']: 
            decrypt_and_print_message(user, user1_sent[u1i], session_key)
            u1i += 1

    # Handle any extra messages that might be in user2_sent or user1_sent
    while u2i < len(user2_sent):
        decrypt_and_print_message(user2, user2_sent[u2i], session_key)
        u2i += 1
    
    while u1i < len(user1_sent):
        decrypt_and_print_message(user, user1_sent[u1i], session_key)
        u1i += 1
    
    return True


def main():
    global user, private_key, public_key

    # Load public key and private key
    private_key, public_key = tui_generate_keypair()
    # TODO: Load session keys

    while True:
        # Print main options
        print("""\nSelect an option:
        1. Register a user with a public key
        2. Get the public key of a user
        3. Send a message to a user (API version)
        4. Get open chats
        5. Get messages from a user
        6. TEST whatever
        7. Generate public, private key for registration
        8. Send message
        9. View chat messages
        0. Exit""")
        choice = input_int("> ")

        if choice == 1:
            #res = register(URL, input("Enter a new userid: "), input("Enter a new public key: "))
            res = register(URL, input("Enter a new userid: "), public_key)
            if res is not None:
                user = res
        elif choice == 2:
            get_key(URL, input("Enter a userid to get the key of: "))
        elif choice == 3:
            if user is None or user == "":
                print("Error: Register or set a user first")
                continue
            # TODO: We'll have to handle this in a more sophisticated way once we have encryption
            send(URL, user, input("Input the recipient userid: "), input("Input the message to send: "))
        elif choice == 4:
            if user is None or user == "":
                print("Error: Register or set a user first")
                continue
            get_chats(URL, user)
        elif choice == 5:
            # TODO: I've realized that any user can request the messages for any
            # other user.  We might want to have some kind of
            # authentication/authorization mechanism
            if user is None or user == "":
                print("Error: Register or set a user first")
                continue
            get_messages(URL, user, input("Input the sender you want to see messages from: "))
        elif choice == 6:
            print(has_chat_started(input("Enter user1: "), input("Enter user2: ")))
        elif choice == 7:
            private_key, public_key = ccakem.kem_keygen512()
            print("Your keys have been generated and stored in program memory.")
        elif choice == 8:
            # Send a message (non-API version)
            res = send_message(input("Enter userid to message: "), input("Input message to send user: "))
            print("Message sent successfully" if res else "Failed to send message")
        elif choice == 9:
            # View all chat messages
            res = view_chat_messages(input("Enter user to show chat for: "))
        elif choice == 0:
            break
        else:
            continue


if __name__ == '__main__':
    main()