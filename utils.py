def bytes_to_signed_ints(byte_data):
    return [(x if x < 128 else x - 256) for x in byte_data]

def bytes_from_signed_ints(signed_ints):
    return bytes((x % 256 for x in signed_ints))
