import requests
import json
from datetime import datetime

def api_register(url, userid, public_key):
    response = requests.post(url + "register",
                  headers={"Content-Type": "application/json"},
                  data=json.dumps({"userid": userid, "public_key": public_key}))
    if response.status_code == 200:
        return userid
    else:
        # TODO: If the public_key is the same as the already registered user's
        # public key, we should consider this user registered
        return None

def api_get_key(url, userid):
    response = requests.post(url + "get_key",
                  headers={"Content-Type": "application/json"},
                  data=json.dumps({"userid": userid}))
    if response.status_code == 200:
        j = json.loads(response.text)
        return j['public_key']
    else:
        return None

def api_send(url, senderid, recipientid, encrypted_text):
    response = requests.post(url + "send",
                  headers={"Content-Type": "application/json"},
                  data=json.dumps({"senderid": senderid, "recipientid": recipientid, "encrypted_text": encrypted_text}))
    if response.status_code == 200:
        return True
    else:
        return False

def api_get_chats(url, userid):
    response = requests.post(url + "get_chats",
                  headers={"Content-Type": "application/json"},
                  data=json.dumps({"userid": userid}))
    if response.status_code == 200:
        senders = json.loads(response.text)["senders"]
        return senders
    else:
        return None

def api_get_messages(url, userid, senderid):
    response = requests.post(url + "get_messages",
                  headers={"Content-Type": "application/json"},
                  data=json.dumps({"userid": userid, "senderid": senderid}))
    if response.status_code == 200:
        json_res = json.loads(response.text)
        if json_res == {}: return []
        return json.loads(response.text)["messages"]
    else:
        return None